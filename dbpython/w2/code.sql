create table Ages (
  name varchar(128),
  age integer
);

delete from Ages;
insert into Ages (name,age) values ('Jensine',36);
insert into Ages (name,age) values ('Abrar',27);
insert into Ages (name,age) values ('Levi',23);
insert into Ages (name,age) values ('Cailin',30);
insert into Ages (name,age) values ('Alanda',18);

select hex(name || age) as X from Ages order by X;
