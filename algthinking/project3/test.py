
from alg_cluster import Cluster
from cpair import closest_pair_strip

test_input = (
    [
        Cluster(set([]), 0, 0, 1, 0), 
        Cluster(set([]), 1, 0, 1, 0), 
        Cluster(set([]), 2, 0, 1, 0), 
        Cluster(set([]), 3, 0, 1, 0)
    ], 1.5, 1.0)

test_input2 = ([Cluster(set([]), 1.0, 1.0, 1, 0), Cluster(set([]), 1.0, 5.0, 1, 0), Cluster(set([]), 1.0, 4.0, 1, 0), Cluster(set([]), 1.0, 7.0, 1, 0)], 1.0, 3.0)


print closest_pair_strip(*test_input2)
