(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

fun reverse(alist) =
    let
        fun aux(alist,acc) =
            case alist of
                [] => acc
              | first::rest => aux(rest,first::acc)
    in
        aux(alist,[])
    end

(* put your solutions for problem 1 here *)

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw

exception IllegalMove

fun all_except_option (astring: string, alos: string list) =
    let
        fun aux(alos: string list,acc: string list, has_astring: bool) =
            case alos of
                  [] => (reverse(acc),has_astring)
                | hd::tl =>
                  let
                      val same_strings = same_string(astring,hd)
                      val has_astring = if same_strings then same_strings else has_astring
                      val next = if same_strings then aux(tl,acc,has_astring) else aux(tl,hd::acc,has_astring)
                  in
                      next
                  end
        val result = aux(alos,[],false)
    in
        case result of
            (_,false) => NONE
          | (xs,_) => SOME (xs)
    end

val test11 = all_except_option ("string", ["string"]) = SOME []
val test12 = all_except_option ("string", ["string","hello","world"]) = SOME ["hello","world"]

fun get_substitutions1(alolos: string list list, astring: string) =
    case alolos of
        [] => []
      | first::rest => let val except = all_except_option(astring,first)
        in case except of
            NONE => get_substitutions1(rest,astring)
          | SOME alos => alos @ get_substitutions1(rest,astring)
        end

val test2 = get_substitutions1 ([["foo"],["there"]], "foo") = []

fun get_substitutions2(alolos: string list list, astring: string) = let
    fun aux(alolos: string list list, acc: string list) =
        case alolos of
            [] => acc
          | first::rest => let val except = all_except_option(astring,first)
            in
                case except of
                    NONE => aux(rest,acc)
                  | SOME alos => aux(rest, alos @ acc)
            end
    in
        aux(alolos,[])
    end

val test3 = get_substitutions2 ([["foo"],["there"]], "foo") = []
val test31 = get_substitutions2 ([["foo","yeah"],["there"]], "foo") = ["yeah"]

fun similar_names(alolos: string list list,{first:string,middle:string,last:string}) =
    let
        fun aux(alolos: string list list, acc) =
            case alolos of
                [] => acc
                | names::rest => let
                val alternative_names = all_except_option(first,names)
                fun resolve_option result =
                    case result of
                        NONE => []
                        | SOME alos => let
                        fun build_record_list(alos: string list, acc) =
                            case alos of
                                [] => acc
                              | alt_name::rest => build_record_list(rest,{first=alt_name,middle=middle,last=last}::acc)
                        in build_record_list(alos,[]) end

                in aux(rest,resolve_option(alternative_names) @ acc) end
    in
        reverse(aux(alolos,[{first=first,middle=middle,last=last}]))
    end

val test4 = similar_names ([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]], {first="Fred", middle="W", last="Smith"}) =
        [{first="Fred", last="Smith", middle="W"}, {first="Fredrick", last="Smith", middle="W"},
         {first="Freddie", last="Smith", middle="W"}, {first="F", last="Smith", middle="W"}]

(* put your solutions for problem 2 here *)

fun card_color (acard: card): color =
    case acard of
        (Hearts,_) => Red
      | (Diamonds,_) => Red
      | (_,_) => Black

val c1 = (Clubs,Num 10)
val c2 = (Diamonds,Num 10)
val test0 = card_color c1 = Black
val test01 = card_color c2 = Red

fun card_value (acard: card): int =
    case acard of
        (_,Num i) => i
      | (_,Ace) => 11
      | (_,_) => 10

fun remove_card(aloc: card list, acard: card, e): card list =
    let
        fun aux(aloc: card list, has_acard: bool, acc: card list): card list * bool =
            case (aloc,has_acard) of
                ([],x) => (acc,x)
              | (c::cs,true) => aux(cs,true,c::acc)
              | (c::cs,_) => if c=acard
                  then aux(cs,true,acc)
                  else aux(cs,false,c::acc)

        val result = aux(aloc,false,[])
    in
        case result of
            (alist,false) => raise e
          | (alist,_) => reverse(alist)
    end

val test7 = remove_card ([(Hearts, Ace)], (Hearts, Ace), IllegalMove) = []
val test71 = remove_card ([(Diamonds,Queen),(Hearts, Ace),(Hearts, Ace)], (Hearts, Ace), IllegalMove) =
    [(Diamonds,Queen),(Hearts, Ace)]

(* http://stackoverflow.com/questions/8560300/sml-function-with-multiple-outputs *)

fun all_same_color(aloc: card list): bool =
  case aloc of
      [] => true
    | c::[] => true
    | c1::c2::[] => card_color(c1) = card_color(c2)
    | c1::c2::rest => if card_color(c1) = card_color(c2)
      then all_same_color(c2::rest) else false

val test8 = all_same_color [(Hearts, Ace), (Hearts, Ace)] = true

fun sum_cards(aloc: card list): int =
    let
      fun aux(aloc: card list, acc: int): int =
          case aloc of
              [] => acc
            | c::cs => aux(cs,card_value(c)+acc)
    in
        aux(aloc,0)
    end
    
fun score(aloc: card list, goal: int): int =
    let
        val the_sum = sum_cards(aloc)
        val cards_are_of_the_same_color = all_same_color(aloc)
        val preliminary_score = if the_sum > goal
            then (sum - goal) else (goal - sum)
    in
        if cards_are_of_the_same_color
        then preliminary_score div 2
        else preliminary_score
    end

fun officiate(aloc: card list, alom: move list, goal: int): int =
    let
        fun aux(deck: card list, held: card list, moves: move list): int =
            case moves of
                [] => score(held,goal)
              | Discard(c)::rest => aux(deck,remove_card(held,c,IllegalMove),rest)
              | Draw::rest => case deck of
                  [] => score(held,goal)
                | c::cs => if sum_cards(c::held) > goal
                    then score(c::held,goal)
                    else aux(cs,c::held,rest)
    in
        aux(aloc,[],alom)
    end
