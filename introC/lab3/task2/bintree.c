#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "bintree.h"

///*** DO NOT CHANGE ANY FUNCTION DEFINITIONS ***///

// Recall node is defined in the header file
node *root = NULL;

// Insert a new node into the binary tree with node_id and data
void insert_node(int node_id, int data) {

	node *current;	
	node *previous;
	node *next;

	node * new_node = (node *) malloc(sizeof(node));
	new_node->id = node_id;
	new_node->data = data;
    new_node->left = NULL;
    new_node->right = NULL;

    if(root==NULL) {
        root = new_node;
    } else {
        for(current=root;current!=NULL;current=next) {
            if(current->id > node_id) {
                next = current->left;	
            } else {
                next = current->right;	
            }	
            previous = current;
        }
        if(previous->id > node_id) {
            previous->left = new_node;	
        } else {
            previous->right = new_node;
        }
    }
}

// Find the node with node_id, and return its data
int find_node_data(int node_id) {
    node * current;
    node * next;
    for(current=root;current!=NULL;current=next) {
        if(current->id==node_id) {
            return current->data;
        } else {
            if(current->id > node_id) {
                next = current->left;
            } else {
                next = current->right;
            }
        }
    }
    return 0;	
}

node * find_min(node * rt) {
    node * current;
    for(current=rt;current->left!=NULL;current=current->left) {

    }
    return current;
}
node * find_abs_min() {
    return find_min(root);
}

//Find and remove a node in the binary tree with node_id. 
//Children nodes are fixed appropriately.
void remove_node(int node_id) {
    node * previous; int direction;
    node * current;
    node * next;
    for(current=root;current!=NULL;current=next) {
        if(current->id==node_id) {
            if(current->left!=NULL&&current->right!=NULL) {
                previous = find_min(current->right);
                int id = previous->id;
                int data = previous->data;
                remove_node(previous->id);
                current->id = id; 
                current->data = data;
                break;
            } else if(current->left==NULL&&current->right==NULL) {
                if(direction==0) {
                    previous->left = NULL;
                } else {
                    previous->right = NULL;
                } 
                free(current);
                break;
            } else if (current->left==NULL) {
                if(direction==0) {
                    previous->left = current->right;
                } else {
                    previous->right = current->right; 
                }
                free(current);
                break;
            } else if (current->right==NULL) {
                if(direction==0) {
                    previous->left = current->left;
                } else {
                    previous->right = current->left; 
                }
                free(current);
                break;
            } else {

            }
        } else {
            if(current->id > node_id) {
                next = current->left;
                direction = 0;
            } else {
                next = current->right;
                direction = 1;
            }
        }
        previous=current;
    }
}

void print_adjacency_list_aux(node * current) {
    if(current!=NULL) {
        node *left = current->left;
        node *right = current->right;
        if(left!=NULL&&right!=NULL) {
            printf("%d %d %d\n",current->id,left->id,right->id);
        } else if(left!=NULL&&right==NULL) {
            printf("%d %d\n",current->id,left->id);
        } else if(left==NULL&&right!=NULL) {
            printf("%d %d\n",current->id,right->id);
        } else if(left==NULL&&right==NULL) {
            printf("%d\n",current->id);
        }
        print_adjacency_list_aux(current->left);
        print_adjacency_list_aux(current->right);
    }    
}

void print_adjacency_list() {
    print_adjacency_list_aux(root);
}
