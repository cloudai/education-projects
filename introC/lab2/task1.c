#include <stdio.h>

void square(int * n) {
    *n = *n * *n;
}

int main() {
    int x = 4;
    square(&x);
    printf("%d\n",x);
    return 0;
}
