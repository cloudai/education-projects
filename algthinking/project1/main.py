""" Project 1 """

EX_GRAPH0 = {
    0:set([1,2]),
    1:set([]),
    2:set([])
}

EX_GRAPH1 = {
    0:set([1,4,5]),
    1:set([2,6]),
    2:set([3]),
    3:set([0]),
    4:set([1]),
    5:set([2]),
    6:set([]),
}

EX_GRAPH2 = {
    0:set([1,4,5]),
    1:set([2,6]),
    2:set([3,7]),
    3:set([7]),
    4:set([1]),
    5:set([2]),
    6:set([]),
    7:set([3]),
    8:set([1,2]),
    9:set([0,3,4,5,6,7]),
}

def make_complete_graph(num_nodes):
    """
    number -> dictionary
    create a complete directed graph 
    """
    graph = {}
    for node in range(num_nodes): 
        graph[node] = set(filter(lambda e: e!=node,range(num_nodes)))
    return graph

def compute_in_degrees(digraph):
    """
    graph -> dictionary
    compute in-degree of each node
    """
    result = {}
    for archs in digraph.iteritems():
        if archs[0] not in result:
            result[archs[0]] = 0
        for head in archs[1]:
            if head in result:
                result[head]+=1
            else:
                result[head]=1
    return result

def in_degree_distribution(digraph):
    """
    graph -> dictionary
    compute in-degree distribution
    """
    result = {}
    for pair in compute_in_degrees(digraph).iteritems():
        if pair[1] in result:
            result[pair[1]] += 1
        else:
            result[pair[1]] = 1 
    return result

