
class BankAccount:
    """ Class definition modeling the behavior of a simple bank account """

    def __init__(self, initial_balance):
        """Creates an account with the given balance."""
	self.balance = initial_balance
	self.total_fees = 0

    def deposit(self, amount):
        """Deposits the amount into the account."""
	self.balance+=amount

    def withdraw(self, amount):
        """
        Withdraws the amount from the account.  Each withdrawal resulting in a
        negative balance also deducts a penalty fee of 5 dollars from the balance.
        """
	new_balance = self.balance - amount
	if(new_balance<0): 
	    new_balance-=5
	    self.total_fees+=5
	self.balance = new_balance

    def get_balance(self):
        """Returns the current balance in the account."""
        return self.balance

    def get_fees(self):
        """Returns the total fees ever deducted from the account."""
	return self.total_fees
