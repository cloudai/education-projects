import getopt,sys
import matplotlib.pyplot as plt
from citation_graph import citation_graph_in_degree as cg_in_degree
from random_graph import random_in_degree
from sdg import sdg_in_degree

def render_plot(in_degree,plot,suffix):
    xvalues = []
    yvalues = []

    def fillValues(point):
        xvalues.append(point[0])
        yvalues.append(point[1])

    [ fillValues(point) for point in in_degree["normalized"].iteritems() ]
    plot(xvalues,yvalues,"bo",label="in-degree")
    plt.title(in_degree["title"])
    plt.xlabel("Number of in-degrees")
    plt.ylabel("Number of occurrences (normalized value)")
    plt.legend()
    #plt.show()
    plt.savefig("../Downloads/image%s.png" % suffix,dpi=60)

def main():
	try:
		opts,args = getopt.getopt(sys.argv[1:],"crs")
	except getopt.GetoptError as err:
		print str(err)
		sys.exit(2)
	for o,a in opts:
		if o == "-c":
			render_plot(cg_in_degree(),plt.loglog,"_c")
		elif o == "-r":
			render_plot(random_in_degree(),plt.plot,"_r")
		elif o == "-s":
			render_plot(sdg_in_degree(),plt.loglog,"_s")
		else:
			assert False, "unhandled"
			
if __name__ == "__main__":
	main()
