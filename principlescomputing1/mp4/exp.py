from code import gen_all_sequences as gas, score, expected_value as exv, gen_all_holds as gah, gen_subsets2 as gs2

#print exv((3,3),8,5)

#print score((2,3,2,2))

#print len(gas(range(1,7),2))

#print gah((5,5))

#print set([ tuple(x) for x in gs2((5,5))])

#print reduce(
#          lambda prev,cur: \
#              cur if cur[0] > prev[0] else prev,
#          [(4,2),(6,0),(1,100)])