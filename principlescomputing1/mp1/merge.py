""" Mini-project 1
    for Principles of Computing (Coursera, Rice)
"""

def merge(alon):
    """
    number* -> number*
    find pairs of equal numbers and
    add them from left to right
    """

    result = len(alon)*[0]
    local = {'r_index':0,'head':None}

    def aux_merge(aux_alon):
        """ number* -> number*
        """
        if len(aux_alon) == 0:
            if local['head']!=None:
                result[local['r_index']] = local['head']
        else:
            if aux_alon[0] == 0:
                aux_merge(aux_alon[1:])
            else:
                if local['head'] == None: 
                    local['head'] = aux_alon[0]
                else:
                    if aux_alon[0] == local['head']:
                        result[local['r_index']] = local['head']*2
                        local['r_index'] += 1
                        local['head'] = None
                    else:
                        result[local['r_index']] = local['head']
                        local['r_index'] += 1
                        local['head'] = aux_alon[0]
                aux_merge(aux_alon[1:])
                        
    aux_merge(alon)
    return result

#assert merge([2,0,2,4]) == [4,4,0,0]
#assert merge([0,0,2,2]) == [4,0,0,0]
#assert merge([2,2,0,0]) == [4,0,0,0]
#assert merge([2,2,2,2,2]) == [4,4,2,0,0]
#assert merge([8,16,16,8]) == [8,32,8,0]

