import random as rand
import string, math

class AdoptionCenter:
    """
    The AdoptionCenter class stores the important information that a
    client would need to know about, such as the different numbers of
    species stored, the location, and the name. It also has a method to adopt a pet.
    """
    def __init__(self, name, species_types, location):
        self.name = name
        self.species_types = species_types
        self.location = tuple([ float(x) for x in location ])
        
    def get_number_of_species(self, animal):
        return self.species_types[animal] \
            if animal in self.species_types else 0
        
    def get_location(self):
        return self.location
        
    def get_species_count(self):
        return dict(self.species_types)
        
    def get_name(self):
        return self.name
        
    def adopt_pet(self, species):
        self.species_types[species] -= 1
        if self.species_types[species] == 0:
            del self.species_types[species]


class Adopter:
    """
    Adopters represent people interested in adopting a species.
    They have a desired species type that they want, and their score is
    simply the number of species that the shelter has of that species.
    """
    def __init__(self, name, desired_species):
        self.name = name
        self.desired_species = desired_species
        
    def get_name(self):
        return self.name
        
    def get_desired_species(self):
        return self.desired_species
        
    def get_score(self, adoption_center):
        return 1.0*adoption_center.get_number_of_species(self.desired_species)



class FlexibleAdopter(Adopter):
    """
    A FlexibleAdopter still has one type of species that they desire,
    but they are also alright with considering other types of species.
    considered_species is a list containing the other species the adopter will consider
    Their score should be 1x their desired species + .3x all of their desired species
    """
    # Your Code Here, should contain an __init__ and a get_score method.
    def __init__(self,name,desired_species,considered_species):
        Adopter.__init__(self,name,desired_species)
        self.considered_species = considered_species
        
    def get_score(self,adoption_center):
        return Adopter.get_score(self,adoption_center) + \
            reduce(
              lambda pr,cur: pr + adoption_center.get_number_of_species(cur),
              self.considered_species,0)*0.3


class FearfulAdopter(Adopter):
    """
    A FearfulAdopter is afraid of a particular species of animal.
    If the adoption center has one or more of those animals in it, they will
    be a bit more reluctant to go there due to the presence of the feared species.
    Their score should be 1x number of desired species - .3x the number of feared species
    """
    # Your Code Here, should contain an __init__ and a get_score method.
    def __init__(self,name,desired_species,feared_species):
        Adopter.__init__(self,name,desired_species)
        self.feared_species = feared_species
        
    def get_score(self,adoption_center):
        score = Adopter.get_score(self,adoption_center) - \
            0.3*adoption_center.get_number_of_species(self.feared_species)
        return 0.0 if score < 0 else score

class AllergicAdopter(Adopter):
    """
    An AllergicAdopter is extremely allergic to a one or more species and cannot
    even be around it a little bit! If the adoption center contains one or more of
    these animals, they will not go there.
    Score should be 0 if the center contains any of the animals, or 1x number of desired animals if not
    """
    # Your Code Here, should contain an __init__ and a get_score method.
    def __init__(self,name,desired_species,allergic_species):
        Adopter.__init__(self,name,desired_species)
        self.allergic_species = allergic_species

    def get_score(self,adoption_center):
        isAllergicToAdoptionCenter = 1
        for species in self.allergic_species:
            if adoption_center.get_number_of_species(species) > 0:
                isAllergicToAdoptionCenter = 0
                break
        return Adopter.get_score(self,adoption_center) * isAllergicToAdoptionCenter


class MedicatedAllergicAdopter(AllergicAdopter):
    """
    A MedicatedAllergicAdopter is extremely allergic to a particular species
    However! They have a medicine of varying effectiveness, which will be given in a dictionary
    To calculate the score for a specific adoption center, we want to find what is the most allergy-inducing species that the adoption center has for the particular MedicatedAllergicAdopter.
    To do this, first examine what species the AdoptionCenter has that the MedicatedAllergicAdopter is allergic to, then compare them to the medicine_effectiveness dictionary.
    Take the lowest medicine_effectiveness found for these species, and multiply that value by the Adopter's calculate score method.
    """
    # Your Code Here, should contain an __init__ and a get_score method.
    def __init__(self,name,desired_species,allergic_species,medicine_effectiveness):
        Adopter.__init__(self,name,desired_species)
        self.allergic_species = allergic_species
        self.medicine_effectiveness = medicine_effectiveness

    def get_score(self,adoption_center):
        minEffectiveness = 1
        for species in self.allergic_species:
            if adoption_center.get_number_of_species(species) > 0:
                if self.medicine_effectiveness[species] < minEffectiveness:
                    minEffectiveness = self.medicine_effectiveness[species]
        return Adopter.get_score(self,adoption_center) * minEffectiveness




class SluggishAdopter(Adopter):
    """
    A SluggishAdopter really dislikes travelleng. The further away the
    AdoptionCenter is linearly, the less likely they will want to visit it.
    Since we are not sure the specific mood the SluggishAdopter will be in on a
    given day, we will asign their score with a random modifier depending on
    distance as a guess.
    Score should be
    If distance < 1 return 1 x number of desired species
    elif distance < 3 return random between (.7, .9) times number of desired species
    elif distance < 5. return random between (.5, .7 times number of desired species
    else return random between (.1, .5) times number of desired species
    """
    # Your Code Here, should contain an __init__ and a get_score method.
    def __init__(self,name,desired_species,location):
        Adopter.__init__(self,name,desired_species)
        self.location = location
    
    def get_linear_distance(self,to_location):
        return math.sqrt(
            sum(map(lambda x,y:(x-y)**2,to_location,self.location)))
            
    def get_score(self,adoption_center):
        dist = self.get_linear_distance(adoption_center.get_location())
        if dist < 1:
            return Adopter.get_score(self,adoption_center)
        if dist < 3:
            return Adopter.get_score(self,adoption_center) * \
                (0.7 + rand.random()*0.2)
        if dist < 5:
            return Adopter.get_score(self,adoption_center) * \
                (0.5 + rand.random()*0.2)
        if dist >= 5:
            return Adopter.get_score(self,adoption_center) * \
                (0.1 + rand.random()*0.4)

    
def get_ordered_adoption_center_list(adopter, list_of_adoption_centers):
    """
    The method returns a list of an organized adoption_center such that the scores for each AdoptionCenter to the Adopter will be ordered from highest score to lowest score.
    """
    # Your Code Here
    def aux(score1,score2):
        if score1[0] > score2[0]:
           return -1
        elif score1[0] < score2[0]:
            return 1
        else:
            if score1[1] > score2[1]:
                return 1
            elif score1[1] < score2[1]:
                return -1
            else:
                return 0
                
    alist = [ (adopter.get_score(a),a.get_name(),a)
        for a in list_of_adoption_centers ]
    alist.sort(cmp=aux)
    return [a[2] for a in alist]

def get_adopters_for_advertisement(adoption_center, list_of_adopters, n):
    """
    The function returns a list of the top n scoring Adopters from list_of_adopters (in numerical order of score)
    """
    def aux(score1,score2):
        if score1[0] > score2[0]:
           return -1
        elif score1[0] < score2[0]:
           return 1
        else:
           if score1[1] > score2[1]:
               return 1
           else:
               return -1
            
    alist = [ (a.get_score(adoption_center),a.get_name(),a)
        for a in list_of_adopters ]
    alist.sort(cmp=aux)
    return [a[2] for a in alist][:n]

 
 
 
 
 
adopter = MedicatedAllergicAdopter("One", "Cat", ['Dog', 'Horse'], {"Dog": .5, "Horse": rand.random()})
adopter2 = Adopter("Two", "Cat")
adopter3 = FlexibleAdopter("Three", "Horse", ["Lizard", "Cat"])
adopter4 = FearfulAdopter("Four","Cat","Dog")
adopter5 = SluggishAdopter("Five","Cat", (1,2))
adopter6 = AllergicAdopter("Six", "Cat", "Dog")

ac4 = AdoptionCenter("Place4", {"Cat": rand.randint(20, 50), "Horse": rand.randint(1, 10)}, (-3,0))
ac5 = AdoptionCenter("Place5", {"Dog": rand.randint(20, 50), "Lizard": rand.randint(1, 10)}, (8,-2))
ac6 = AdoptionCenter("Place6", {"Cat": rand.randint(20, 50), "Dog": rand.randint(1, 10), "Horse": rand.randint(1, 10)}, (-10,10))

for i in xrange(20):
  alist = get_adopters_for_advertisement(ac4,[adopter, adopter2, adopter3, adopter4, adopter5, adopter6],10)
  print alist

adopter4 = FearfulAdopter("Four","Cat","Dog")
adopter5 = SluggishAdopter("Five","Cat", (1,2))
adopter6 = AllergicAdopter("Six", "Lizard", "Cat")

ac = AdoptionCenter("Place1", {"Cat": 12, "Dog": 2}, (1,1))
ac2 = AdoptionCenter("Place2", {"Cat": 12, "Lizard": 2}, (3,5))
ac3 = AdoptionCenter("Place3", {"Cat": rand.randint(20, 50), "Dog": rand.randint(1, 10)}, (-2,10))
ac4 = AdoptionCenter("Place4", {"Cat": rand.randint(20, 50), "Horse": rand.randint(1, 10)}, (-3,0))
ac5 = AdoptionCenter("Place5", {"Cat": rand.randint(20, 50), "Lizard": rand.randint(1, 10)}, (8,-2))
ac6 = AdoptionCenter("Place6", {"Cat": rand.randint(20, 50), "Dog": rand.randint(1, 10), "Horse": rand.randint(1, 10)}, (-10,10))

for i in xrange(20):
  alist = get_ordered_adoption_center_list(adopter4,[ac,ac2,ac3,ac4,ac5,ac6])
  print alist