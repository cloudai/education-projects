""" 
    Synthetic directed graph
"""
import random
from ..project1.main import make_complete_graph,compute_in_degrees
from citation_graph import build_distribution
from DPATrial import DPATrial

def sdg(n,m):
    """ number number -> dictionary """

    graph = make_complete_graph(m)
    d = DPATrial(len(graph))
    for i in range(m,n):
       graph[i] = d.run_trial(len(graph))

    return graph

def sdg_in_degree():
	return build_distribution(
		#sdg(27770,12),
		sdg(2500,7),
		"Synthetic directed graph in-degree distribution")

if __name__ == "__main__":
	print sdg(10,5)
