"""
Planner for Yahtzee
Simplifications:  only allow discard and roll, only score against upper level
"""

# Used to increase the timeout, if necessary
import codeskulptor
codeskulptor.set_timeout(20)

#res = []
#def gen_subsets(aset,acc=[]):
#   """ list list -> list """
#   if len(aset) == 0:
#       res.append(acc)
#   else:
#       gen_subsets(aset[1:],acc)
#       gen_subsets(aset[1:],acc+aset[:1])
#
#gen_subsets([1,2])

def gen_all_sequences(outcomes, length):
    """
    Iterative function that enumerates the set of all sequences of
    outcomes of given length
    """
    
    ans = set([()])
    for dummy_idx in range(length):
        temp = set()
        for seq in ans:
            for item in outcomes:
                new_seq = list(seq)
                new_seq.append(item)
                temp.add(tuple(new_seq))
        ans = temp
    return ans
    
def score(hand):
    """ tuple -> integer """
    scores = {}
    
    def aux1(key):
        """ number -> void """
        if key in scores:
            scores[key]+=1
        else:
            scores[key]=1
        
    dummy_var1 = map(aux1,hand)
    
    return max([ x[0]*x[1] for x in scores.items() ])
    
def expected_value(held_dice, num_die_sides, num_free_dice):
    """
    Compute the expected value based on held_dice given that there
    are num_free_dice to be rolled, each with num_die_sides.

    held_dice: dice that you will hold
    num_die_sides: number of sides on each die
    num_free_dice: number of dice to be rolled

    Returns a floating point expected value
    
    """
    omega = float(num_die_sides ** num_free_dice)
    
    return sum([ score(x+held_dice) for x in
        gen_all_sequences(
            range(1,num_die_sides+1),num_free_dice)]) / (omega)


def gen_subsets2(aset,acc):
    """ list||tuple list -> list """
    if len(aset) == 0:
        return [acc]
    else:
        return gen_subsets2(aset[1:],acc) \
            + gen_subsets2(aset[1:],acc+[aset[0]])

def gen_all_holds(ahand):
    """ tuple -> set-of-tuples """
    return set([ tuple(x) #discard_subset(ahand,x)
        for x in gen_subsets2(ahand,[])])

def strategy(hand,num_die_sides):
    """ tuple number -> tuple """
    
    return reduce(
      
        lambda prev,cur: \
            cur if cur[0] > prev[0] else prev,
            
        [ (expected_value(
            ahold,num_die_sides,len(hand)-len(ahold)),ahold)
            
            for ahold in gen_all_holds(hand) ])


#print score((2,2,6,2,2))
#print discard_subset((1,2,2,3),[])
#print gen_all_holds((1,2,2,3))
#print strategy((1,1,1,1,4),6)
