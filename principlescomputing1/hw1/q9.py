
def appendsum(lst,reps=25):
    """
    Repeatedly append the sum of the
    current last three elements of lst to lst
    """
    for rep in range(reps):
        lst.append(sum(lst[-3:]))

sum_three = [0,1,2]

appendsum(sum_three)


print sum_three[10]
print sum_three[20]
