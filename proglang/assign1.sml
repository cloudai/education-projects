
fun is_older(date1: int*int*int, date2: int*int*int) =
    if (#1 date1) < (#1 date2)
    then true 
    else if (#1 date1) > (#1 date2) then false
         else if (#2 date1) < (#2 date2) then true
              else if (#2 date1) > (#2 date2) then false
                   else if (#3 date1) < (#3 date2) then true
                        else if (#3 date1) > (#3 date2) then false
                        else false

(* val test1 = is_older ((1,2,3),(2,3,4)) = true *)

fun number_in_month(alod: (int*int*int) list,month: int) =
    if null alod then 0
    else if (#2 (hd alod)) = month then 1 + number_in_month(tl alod,month)
    else number_in_month(tl alod,month)

(* val test2 = number_in_month ([(2012,2,28),(2013,12,1)],2) = 1 *)

fun number_in_months(alod: (int*int*int) list,alom: int list) =
    if null alom then 0 
    else number_in_month (alod,hd alom) + number_in_months(alod,tl alom)

(* val test3 = number_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = 3 *)

fun dates_in_month (alod: (int*int*int) list, month: int) = 
    if null alod then [] 
    else if (#2 (hd alod)) = month then (hd alod) :: dates_in_month (tl alod,month)
    else dates_in_month (tl alod,month)

(* val test4 = dates_in_month ([(2012,2,28),(2013,12,1)],2) = [(2012,2,28)] *)

fun dates_in_months (alod: (int*int*int) list,alom: int list) = 
    if null alom then []
    else dates_in_month(alod,hd alom) @ dates_in_months(alod,tl alom)

(* val test5 = dates_in_months ([(2012,2,28),(2013,12,1),(2011,3,31),(2011,4,28)],[2,3,4]) = [(2012,2,28),(2011,3,31),(2011,4,28)] *)

fun get_nth (alos: string list, n: int) =
    if n = 1 then hd alos else get_nth (tl alos,n-1)

(* val test6 = get_nth (["hi", "there", "how", "are", "you"], 2) = "there" *)

fun date_to_string (date: (int*int*int)) =
    let val month_names = [
        "January","February","March","April","May","June","July",
        "August","September","October","November","December"]
    in
    get_nth (month_names,(#2 date)) ^ " " ^ Int.toString (#3 date) ^ ", " ^ Int.toString (#1 date)
    end

(* val test7 = date_to_string (2013, 6, 1) = "June 1, 2013" *)

fun number_before_reaching_sum (sum: int, aloi: int list) =
    let fun aux (aloi: int list, list_sum: int, n: int) =
            if null aloi 
            then 0
            else if list_sum + hd aloi >= sum 
                 then n
                 else aux(tl aloi,hd aloi + list_sum,n+1)
    in
    aux(aloi,0,0)
    end

(* val test8 = number_before_reaching_sum (10, [1,2,3,4,5]) = 3 *)

fun what_month(day: int) =
    let val days_in_month = [31,28,31,30,31,30,31,31,30,31,30,31]
    in number_before_reaching_sum (day,days_in_month) + 1
    end

(* val test9 = what_month 70 = 3 *)

fun month_range(day1: int, day2: int) =
    if day1 >= day2 
    then [] 
    else let fun aux(n: int) = 
                 if (n + day1) = day2 
                 then what_month(day2) :: []
                 else what_month(day1+n) :: aux(n+1)
         in 
             aux(0)
         end

(* val test10 = month_range (31, 34) = [1,2,2,2] *)

fun oldest(alod: (int*int*int) list) =
    if null alod then NONE 
    else let fun aux(alod: (int*int*int) list) =
                 if null (tl alod) then hd alod
                 else let val tail_answer = aux(tl alod)
                      in if is_older(hd alod,tail_answer) 
                         then hd alod
                         else tail_answer
                      end
         in SOME (aux alod) end

(* val test11 = oldest([(2012,2,28),(2011,3,31),(2011,4,28)]) = SOME (2011,3,31) *)


