from math import ceil,log

STANDARD = True
LOGLOG = False

def greedy_boss(days_in_simulation, bribe_cost_increment, plot_type = STANDARD):
    """
    Simulation of greedy boss
    """
    
    # initialize necessary local variables
    current_day = 0
    bribe = 1000
    salary = 100
    total_pay = 0
    remainder = 0
    
    days_vs_earnings = []

    while current_day <= days_in_simulation:
      
        record = (current_day,total_pay) if plot_type == STANDARD \
            else tuple(map(lambda e: log(e,10) if e>0 else e,(current_day,total_pay)))
            
        days_vs_earnings.append(record)
        
        day_inc = ceil((bribe-remainder)/salary)
        remainder += day_inc * salary - bribe
        total_pay += salary * day_inc
        
        salary += 100
        bribe += bribe_cost_increment
        current_day+=day_inc
   
    return days_vs_earnings

print greedy_boss(35,100,LOGLOG)
print greedy_boss(35,0)
