""" Mini-project 2

    NEXT
    - define the ending point of list expansion
    
"""
def reverse_list(alist):
    """ list -> list
        create a reversed version of alist
    """
    result_index = len(alist)
    result = [0]*result_index
    for elem in alist:
        result_index = result_index - 1
        result[result_index] = elem
    return result
    
def merge(alon):
    """
    number* -> number*
    find pairs of equal numbers and
    add them from left to right
    NOTE: re-write the procedure as non-recursive
    """

    result = len(alon)*[0]
    local = {'r_index':0,'head':None}

    def aux_merge(aux_alon):
        """ number* -> number*
        """
        if len(aux_alon) == 0:
            if local['head']!=None:
                result[local['r_index']] = local['head']
        else:
            if aux_alon[0] == 0:
                aux_merge(aux_alon[1:])
            else:
                if local['head'] == None:
                    local['head'] = aux_alon[0]
                else:
                    if aux_alon[0] == local['head']:
                        result[local['r_index']] = local['head']*2
                        local['r_index'] += 1
                        local['head'] = None
                    else:
                        result[local['r_index']] = local['head']
                        local['r_index'] += 1
                        local['head'] = aux_alon[0]
                aux_merge(aux_alon[1:])
                        
    aux_merge(alon)
    return result

def make_grid(rows,columns):
    """ number number -> list
        create rows*columns grid and
        fill it with cell indices
    """
    return [ [ (row,col) for col in range(columns) ]
        for row in range(rows) ]

def init_tiles_offset(rows,columns):
    """ number number -> dictionary
        precompute data for initial tiles and offset
        for UP,DOWN,LEFT,RIGHT directions
    """
    up = {
        "init":[(0,x) for x in range(columns)],
        "offset":(1,0),
        "range":rows
    }
    right = {
        "init": [(x,columns-1) for x in range(rows)],
        "offset":(0,-1),
        "range":columns
    }
    down = {
        "init": [(rows-1,x) for x in range(columns)],
        "offset":(-1,0),
        "range":rows
    }
    left = {
        "init":[(x,0) for x in range(rows)],
        "offset":(0,1),
        "range":columns
    }
    return {"UP":up,"DOWN":down,"RIGHT":right,"LEFT":left}


def compute_indices(rows,cols,direction,grid=None):
    """ number number string -> list
        get a collections of indices for the grid
        that form a list for merging values
        
    >>> compute_indices(2,3,"LEFT") # doctest: +SKIP
    [[(0, 0), (0, 1), (0, 2)], [(1, 0), (1, 1), (1, 2)]]
    >>> compute_indices(2,3,"RIGHT") # doctest: +SKIP
    [[(0, 2), (0, 1), (0, 0)], [(1, 2), (1, 1), (1, 0)]]
    >>> compute_indices(2,3,"UP") # doctest: +SKIP
    [[(0, 0), (1, 0)], [(0, 1), (1, 1)], [(0, 2), (1, 2)]]
    >>> compute_indices(2,3,"DOWN") # doctest: +SKIP
    [[(1, 0), (0, 0)], [(1, 1), (0, 1)], [(1, 2), (0, 2)]]
    >>> compute_indices(3,2,"UP") # doctest: +SKIP
    [[(0, 0), (1, 0), (2, 0)], [(0, 1), (1, 1), (2, 1)]]
    >>> grid = [[0,0,2],[0,2,2]]
      >>> compute_indices(2,3,"LEFT",grid)
        >>> grid
        [[2, 0, 0], [4, 0, 0]]
    >>> grid = [[0,0,2],[0,2,2]]
      >>> compute_indices(2,3,"RIGHT",grid)
        >>> grid
        [[0, 0, 2], [0, 0, 4]]
    >>> grid = [[0,0,2],[0,2,2]]
      >>> compute_indices(2,3,"UP",grid)
        >>> grid
        [[0, 2, 4], [0, 0, 0]]
    >>> grid = [[0,0,2],[0,2,2]]
      >>> compute_indices(2,3,"DOWN",grid)
        >>> grid
        [[0, 0, 0], [0, 2, 4]]
    """
    ROWS = rows
    COLS = cols
    DIR = direction
    
    init_offset = init_tiles_offset(ROWS,COLS)
    
    def fix_list(alon):
        """ number* -> void
            get a list of indicies to access the grid
            and "fix" values
        """
        merged = merge([ grid[c_inx[0]][c_inx[1]] for c_inx in alon ])
        
        for m_inx,c_inx in enumerate(alon):
            grid[c_inx[0]][c_inx[1]] = merged[m_inx]
    
    [ fix_list([
        (
          it[0]+init_offset[DIR]["offset"][0]*x,
          it[1]+init_offset[DIR]["offset"][1]*x
        )
        for x in range(init_offset[DIR]["range"]) ])
          for it in init_offset[DIR]["init"] ]
  
if __name__ == "__main__":
    import doctest
    doctest.testmod()
