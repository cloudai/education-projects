import matplotlib.pyplot as plt
import random
#from ..project1.main import in_degree_distribution
from citation_graph import build_distribution

def gen_random_graph(num_nodes,probability):
    graph = {}
    for i in range(num_nodes):
        graph[i] = set([])
        for j in range(num_nodes):
            if i!=j: 
                if random.random() < probability:
                    graph[i].add(j)
    return graph

def random_in_degree():
	return build_distribution(
		gen_random_graph(5000,0.1),
		"Random graph in-degree distribution")
