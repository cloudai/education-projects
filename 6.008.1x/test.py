from simpsons_paradox_data import *

print joint_prob_table
# joint probability gender, admission
PJ_GA = joint_prob_table.sum(axis=1)
P_G = joint_prob_table.sum(axis=(1,2))

# probability of female,admitted
P_fa = PJ_GA[gender_mapping['female'],admission_mapping['admitted']]
P_ma = PJ_GA[gender_mapping['male'],admission_mapping['admitted']]

print 'P(A=admitted|G=female): %.3f' % (P_fa/P_G[0])
print 'P(A=admitted|G=male): %.3f' % (P_ma/P_G[1])

for g_indx,gender in enumerate(['female','male']):
    for d_indx,department in enumerate(['A','B','C','D','E','F']):
        print 'P(A=admitted|G=%s,D=%s): %.3f' % \
            (gender,department,joint_prob_table[g_indx][d_indx][0]/joint_prob_table[g_indx][d_indx].sum())