""" Project 3 / Algorithmic Thinking
"""

def slow_closest_pair(loc):
    """ List-of Cluster -> Tuple
    """
    closest_pair = None
    for index_i,cluster_i in enumerate(loc):
        for index_j,cluster_j in enumerate(loc):
            if index_i != index_j:
                dist = cluster_i.distance(cluster_j) 
                if closest_pair is None:
                    closest_pair = (dist,index_i,index_j)
                else:
                    if dist < closest_pair[0]:
                        closest_pair = (dist,index_i,index_j)
    return closest_pair

def fast_closest_pair(loc):
    """ List-of Cluster -> Tuple
    """
    card_loc = len(loc)
    cpair = None

    if card_loc <= 3:
        cpair = slow_closest_pair(loc)
    else:
        cp_l = fast_closest_pair(loc[0:card_loc/2])
        cp_r = fast_closest_pair(loc[card_loc/2:])
        cpair = cp_l if cp_l[0] < cp_r[0] else \
            (cp_r[0],cp_r[1]+card_loc/2,cp_r[2]+card_loc/2)
        mid = 0.5*(loc[card_loc/2-1].horiz_center()+loc[card_loc/2].horiz_center())
        cpair_strip = closest_pair_strip(loc,mid,cpair[0])
        cpair = cpair if cpair[0] < cpair_strip[0] else cpair_strip 

    return cpair


def closest_pair_strip(loc,horiz_center,half_width):
    """ List-of Cluster float float -> Tuple
    """

    _big_s = [ (i,c) for i,c in enumerate(loc) 
            if abs(c.horiz_center()-horiz_center)<half_width ]

    _big_s.sort(key=lambda e:e[1].vert_center())

    big_s = [ x[0] for x in _big_s ]
    card_s = len(big_s)

    closest_pair = (float('inf'),-1,-1)

    for i_x in range(0,card_s-2+1):
        for j_x in range(i_x+1,min(i_x+3+1,card_s-1+1)):
            if big_s[i_x] < big_s[j_x]:
                dist = (loc[big_s[i_x]].distance(loc[big_s[j_x]]),big_s[i_x],big_s[j_x])
            else:
                dist = (loc[big_s[i_x]].distance(loc[big_s[j_x]]),big_s[j_x],big_s[i_x])
            closest_pair = dist if dist[0] < closest_pair[0] else closest_pair 

    return closest_pair

def hierarchical_clustering(loc,nclust):
    """ List-of Cluster number -> List-of Cluster
    """
    while len(loc) > nclust:
        cpair = slow_closest_pair(loc)
        loc[cpair[1]].merge_clusters(loc[cpair[2]])
        loc.pop(cpair[2])
    return loc


#def kmeans_clustering(loc,num_clusters,num_iter):
   #""" List-of Cluster number number -> List-of Cluster
   #"""
   #card_loc = len(loc)
   #k_centers = list(loc)
   #k_centers.sort(key=lambda e:e.total_population(),reverse=True)
   #k_centers[0:num_clusters+1]

   #for i in range(1,num_iter+1):
   #    kclusters = [Cluster(set([]),0,0,0,0)]*num_cluster
   #    for cluster in loc:
   #        cpair = slow_closest_pair(k_centers[0:num_clusters+1]+cluster)
   #        ind = filter(lambda e:e!=num_cluster+1,cpair[1:])[0]
   #        #kcluster[ind] = cluster.merge(kcluster[ind])
   #        kcluster[ind].merge(cluster)

   #return kclusters

