#include <stdio.h>
#include "bintree.h"

int main() {
	/*
	Insert your test code here. Try inserting nodes then searching for them.

	When we grade, we will overwrite your main function with our own sequence of
	insertions and deletions to test your implementation. If you change the
	argument or return types of the binary tree functions, our grading code
	won't work!
	*/
    insert_node(2,100);
    insert_node(5,110);
    insert_node(4,120);
    insert_node(6,130);
    //int res = find_node_data(5); 
    remove_node(5);
    print_adjacency_list();
	return 0;
}
