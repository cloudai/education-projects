"""
Clone of 2048 game.
"""

import poc_2048_gui
import random

# Directions, DO NOT MODIFY
UP = 1
DOWN = 2
LEFT = 3
RIGHT = 4

# Offsets for computing tile indices in each direction.
# DO NOT MODIFY this dictionary.
OFFSETS = {UP: (1, 0),
           DOWN: (-1, 0),
           LEFT: (0, 1),
           RIGHT: (0, -1)}

def merge(alon):
    """
    number* -> number*
    find pairs of equal numbers and
    add them from left to right
    NOTE: re-write the procedure as non-recursive
    """

    result = len(alon)*[0]
    local = {'r_index':0,'head':None}

    def aux_merge(aux_alon):
        """ number* -> number*
        """
        if len(aux_alon) == 0:
            if local['head']!=None:
                result[local['r_index']] = local['head']
        else:
            if aux_alon[0] == 0:
                aux_merge(aux_alon[1:])
            else:
                if local['head'] == None:
                    local['head'] = aux_alon[0]
                else:
                    if aux_alon[0] == local['head']:
                        result[local['r_index']] = local['head']*2
                        local['r_index'] += 1
                        local['head'] = None
                    else:
                        result[local['r_index']] = local['head']
                        local['r_index'] += 1
                        local['head'] = aux_alon[0]
                aux_merge(aux_alon[1:])
                        
    aux_merge(alon)
    return result

class TwentyFortyEight:
    """
    Class to run the game logic.
    """

    def __init__(self, grid_height, grid_width):
        self._height = grid_height
        self._width = grid_width
        self.init_tiles_offset(grid_height,grid_width)
        self.reset()

    def reset(self):
        """
        Reset the game so the grid is empty except for two
        initial tiles.
        """
        self._grid = [
            [ 0 for _dummy_y in range(self._width) ]
                for _dummy_x in range(self._height) ]
        self.new_tile()
        self.new_tile()

    def __str__(self):
        """
        Return a string representation of the grid for debugging.
        """
        return "["+'\n'.join([ str(x) for x in self._grid ])+"]"

    def get_grid_height(self):
        """
        Get the height of the board.
        """
        return self._height

    def get_grid_width(self):
        """
        Get the width of the board.
        """
        return self._width

    def move(self, direction):
        """
        Move all tiles in the given direction and add
        a new tile if any tiles moved.
        """
        local = {"changed":False}
        def fix_list(alon):
            """ number* -> void
                get a list of indicies to access the grid
                and "fix" values
            """
            sequence = [ self._grid[c_inx[0]][c_inx[1]] for c_inx in alon ]
            merged = merge(sequence)
            if not local["changed"]:
              if sequence != merged:
                  local["changed"] = True
            for m_inx,c_inx in enumerate(alon):
                self._grid[c_inx[0]][c_inx[1]] = merged[m_inx]
      
        dummy_list = [ fix_list([
            (
              it[0]+self._init_offset[direction]["offset"][0]*x,
              it[1]+self._init_offset[direction]["offset"][1]*x
            )
            for x in range(self._init_offset[direction]["range"]) ])
              for it in self._init_offset[direction]["init"] ]
              
        if local["changed"]:
            self.new_tile()
  
    def new_tile(self,count=0):
        """
        Create a new tile in a randomly selected empty
        square.  The tile should be 2 90% of the time and
        4 10% of the time.
        """
        row = random.randint(0,self._height-1)
        col = random.randint(0,self._width-1)
        if self.get_tile(row,col) == 0:
            self.set_tile(row,col,2 if random.random() < 0.9 else 4)
        else:
            if count<50:
              count+=1
              self.new_tile(count)

    def set_tile(self, row, col, value):
        """
        Set the tile at position row, col to have the given value.
        """
        self._grid[row][col] = value

    def get_tile(self, row, col):
        """
        Return the value of the tile at position row, col.
        """
        return self._grid[row][col]
        
    def init_tiles_offset(self,rows,columns):
        """ number number -> dictionary
            precompute data for initial tiles and offset
            for UP,DOWN,LEFT,RIGHT directions
        """
        upp = {
            "init":[(0,x) for x in range(columns)],
            "offset":(1,0),
            "range":rows
        }
        right = {
            "init": [(x,columns-1) for x in range(rows)],
            "offset":(0,-1),
            "range":columns
        }
        down = {
            "init": [(rows-1,x) for x in range(columns)],
            "offset":(-1,0),
            "range":rows
        }
        left = {
            "init":[(x,0) for x in range(rows)],
            "offset":(0,1),
            "range":columns
        }
        self._init_offset = {UP:upp,DOWN:down,RIGHT:right,LEFT:left}


poc_2048_gui.run_gui(TwentyFortyEight(4, 4))
