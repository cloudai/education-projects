"""
    Assignment 1
"""
import string
from ..project1.main import in_degree_distribution


DATA_FILE = "algthinking/assign1/alg_phys-cite.txt" 

def load_graph(file_name):
    graph = {}
    fp = open(file_name,"r")
    for line in fp:
        row = string.rsplit(line)
        graph[row[0]] = set([]) if len(row[1:])==0 else set(row[1:])
    return graph

graph = load_graph(DATA_FILE)

# keys represent number of edges, values represent 
# number of nodes with that amount of edges

def build_distribution(graph,title):
	""" dict number -> dict"""
	distribution = in_degree_distribution(graph)
	total_elements = len(graph)
	return {
		"title": title,
		"raw": distribution,
		"normalized": dict([(x[0],float(x[1])/total_elements) 
			for x in distribution.iteritems()])
	}

def citation_graph_in_degree():
	return build_distribution(
		graph,"Citation graph in-degree distribution")

