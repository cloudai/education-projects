""" Project 2
"""
from collections import deque

def bfs_visited(graph,start_node):
    """ graph number -> set
        return a set of all nodes visited by the subroutine
    """ 
    visited = set([])
    queue = deque([])
    visited.add(start_node)
    queue.append(start_node)

    while len(queue) > 0:
        node =  queue.popleft()
        for head in graph[node]:
            if head not in visited:
                visited.add(head)
                queue.append(head)
    return visited

def cc_visited(graph):
    """ graph -> set
        return a set of connected components of a graph 
    """
    connected_components = []
    rnodes = set(graph.keys())

    while len(rnodes) > 0:
        anode = next(iter(rnodes))
        visited = bfs_visited(graph,anode)
        connected_components.append(visited) 
        rnodes.difference_update(visited)

    return connected_components

def largest_cc_size(graph):
    """ graph -> number
        compute largest connected component
    """
    max_value = 0 
    for component in cc_visited(graph):
        if len(component) > max_value:
            max_value = len(component)
    return max_value

def compute_resilience(graph,attack_order):
    """ graph list -> list
        compute resilience of a graph 
    """

    def remove_node(node):
        """ number -> void
            remove a node from graph 
        """
        graph.pop(node)
        graph.update(
            [ (x[0],filter(lambda e:e!=node,x[1])) 
                for x in graph.iteritems() ])

        return graph

    result = [largest_cc_size(graph)]

    for node in attack_order:
        result.append(
            largest_cc_size(
                remove_node(node)))

    return result

